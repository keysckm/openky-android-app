package kr.ac.konyang.openky;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import kr.ac.konyang.openky.fragments.BaseFragment;

/**
 * Created by KyeongMin on 2015-05-19.
 */
public class NavigationItemView extends LinearLayout {

    private ImageView ivIcon;
    private TextView txtTitle;
    private Class<? extends BaseFragment> execute;

    public NavigationItemView(Context context)
    {
        super(context);

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.listitem_navigation, this, true);

        ivIcon = (ImageView)findViewById(R.id.iconItem);
        txtTitle = (TextView)findViewById(R.id.txtTitle);
    }

    public void setExecute(Class<? extends BaseFragment> cls) { execute = cls; }

    public Class<? extends BaseFragment> getExecute() { return execute; }

    public void setIcon(Drawable icon)
    {
        ivIcon.setImageDrawable(icon);
    }

    public void setTitle(String title)
    {
        txtTitle.setText(title);
    }

    public String getTitle()
    {
        return txtTitle.getText().toString();
    }
}
