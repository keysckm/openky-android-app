package kr.ac.konyang.openky;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.apache.http.HttpResponse;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import kr.ac.konyang.openky.model.Account;
import kr.ac.konyang.openky.model.LoginModelModel;
import kr.ac.konyang.openky.reflect.ModelGenerater;
import kr.ac.konyang.openky.requester.AccountRequest;

public class MainActivity extends ActionBarActivity {

    private ProgressDialog pdlg = null;
    private Button      btnLogin = null;
    private EditText    txtId = null;
    private EditText    txtPw = null;
    private TextView    txtResult = null;

    public static Config config = null;
    public static String baseUrl = null;
    public static Account account = null;
    public static MainActivity self = null;

    private void showProgressDialog(String text) {
        pdlg = ProgressDialog.show(this, "", text, true);
    }

    private void hideProgressDialog() {
        if(pdlg != null)
            pdlg.dismiss();
    }

    protected class LoginTask extends AsyncTask<String, Void, Integer>
    {
        private boolean isToken = false;

        public LoginTask setToken() {
            isToken = true;
            return this;
        }

        @Override
        protected void onPreExecute() {
            showProgressDialog(getString(R.string.status_access));
        }

        @Override
        protected Integer doInBackground(String... params) {

            try {
                HttpResponse rp = null;
                if(!isToken) {
                    LoginModelModel model = new LoginModelModel();
                    model.Id = params[0];
                    model.Password = params[1];
                    model.GcmId = regid;
                    rp = AccountRequest.RequestLogin(model);
                }
                else
                    rp = AccountRequest.RequestTokenLogin(params[0]);

                if(rp == null)
                    return 0;

                String responseString = EntityUtils.toString(rp.getEntity(), HTTP.UTF_8);
                int response_code = rp.getStatusLine().getStatusCode();
                if(response_code == 200) {

                    String msg = responseString + " code: " + response_code;
                    Log.d("login", msg);

                    JSONObject jsons = new JSONObject(responseString);
                    Account model = (Account)ModelGenerater.GenerateFromJson(Account.class, jsons);

                    account = model;
                    config.writeConfig("Token", model.Token);
                }
                return response_code;
            }
            catch(IOException e)
            {
                e.printStackTrace();
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }

            return 0;
        }

        @Override
        protected void onPostExecute(Integer result) {
            hideProgressDialog();
            switch (result)
            {
                case 200:
                    Intent intent = new Intent(MainActivity.this, ContentActivity.class);
                    startActivity(intent);
                    MainActivity.self.finish();
                    return;

                case 403:
                    txtResult.setText(R.string.status_wrong_token);
                    txtId.setEnabled(true);
                    txtPw.setEnabled(true);
                    btnLogin.setEnabled(true);
                    break;

                case 404:
                    txtResult.setText(getString(R.string.status_not_found));
                    break;

                case 500:
                    txtResult.setText(getString(R.string.status_server_error));
                    break;

                case 0:
                    txtResult.setText(getString(R.string.status_server_shutdown));
                    break;

                default:
                    txtResult.setText(R.string.status_unknown_error);
                    break;
            }
            txtResult.setTextColor(0xFFCC2222);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = getApplicationContext();
        config = new Config(getApplicationContext());

        baseUrl = getString(R.string.serverUrl);
        self = this;

        txtId = (EditText)findViewById(R.id.txtId);
        txtPw = (EditText)findViewById(R.id.txtPassword);
        txtResult = (TextView)findViewById(R.id.txtResult);

        btnLogin = (Button)findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new LoginTask().execute(txtId.getText().toString(), txtPw.getText().toString());
            }
        });

        if(checkPlayServices()) {
            gcm = GoogleCloudMessaging.getInstance(this);
            regid = getRegistrationId(context);

            if(regid.isEmpty())
                registerInBackground();
        }
        else
        {
            Toast.makeText(this, "Not support google play service...", Toast.LENGTH_SHORT).show();
        }

        String token = config.readConfig("Token");
        if(token != null && !token.equals("")) {
            btnLogin.setEnabled(false);
            txtId.setEnabled(false);
            txtPw.setEnabled(false);

            new LoginTask().setToken().execute(token);
        }
    }

    private static final String TAG = "OpenKY";

    public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    GoogleCloudMessaging gcm;
    SharedPreferences prefs;
    Context context;
    String regid;

    /**
     * Substitute you own sender ID here. This is the project number you got
     * from the API Console, as described in "Getting Started."
     */
    String SENDER_ID = "864844122078";

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i("ICELANCER", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGCMPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");
        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }

        // 앱이 업데이트 되었는지 확인하고, 업데이트 되었다면 기존 등록 아이디를 제거한다.
        // 새로운 버전에서도 기존 등록 아이디가 정상적으로 동작하는지를 보장할 수 없기 때문이다.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    private SharedPreferences getGCMPreferences(Context context) {
        return getSharedPreferences(MainActivity.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }

    private static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    }
                    regid = gcm.register(SENDER_ID);
                    msg = "Device registered, registration ID=" + regid;

                    // 서버에 발급받은 등록 아이디를 전송한다.
                    // 등록 아이디는 서버에서 앱에 푸쉬 메시지를 전송할 때 사용된다.
                    sendRegistrationIdToBackend();

                    // 등록 아이디를 저장해 등록 아이디를 매번 받지 않도록 한다.
                    storeRegistrationId(context, regid);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                Log.d(TAG, msg);
            }

        }.execute(null, null, null);
    }

    private void storeRegistrationId(Context context, String regid) {
        final SharedPreferences prefs = getGCMPreferences(context);
        int appVersion = getAppVersion(context);
        Log.i(TAG, "Saving regId on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regid);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    private void sendRegistrationIdToBackend() {

    }
}
