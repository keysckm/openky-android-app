package kr.ac.konyang.openky.fragments;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import org.apache.http.HttpResponse;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import kr.ac.konyang.openky.MainActivity;
import kr.ac.konyang.openky.R;
import kr.ac.konyang.openky.model.LogModel;
import kr.ac.konyang.openky.model.LogRequest;
import kr.ac.konyang.openky.reflect.ModelGenerater;
import kr.ac.konyang.openky.requester.LogRequester;

/**
 * Created by KyeongMin on 2015-05-21.
 */
public class FacilityLogFragment extends BaseFragment implements Toolbar.OnMenuItemClickListener{

    private List<LogModel> logs;
    public class LogGetTask extends FragmentAsyncTask<LogRequest, Void> {

        @Override
        protected Integer overrideDoInBackground(LogRequest... params) throws IOException, JSONException, NullPointerException {
            int status = 0;
            HttpResponse rp = LogRequester.RequestLog(params[0]);
            status = rp.getStatusLine().getStatusCode();
            ArrayList<LogModel> lst = new ArrayList<LogModel>();

            if (status == 200) {
                String response_body = EntityUtils.toString(rp.getEntity(), HTTP.UTF_8);
                Log.d("request", response_body + "code: " + rp.getStatusLine().getStatusCode());

                JSONArray arry = new JSONArray(response_body);
                for (int i = 0; i < arry.length(); i++) {
                    JSONObject json = arry.getJSONObject(i);
                    LogModel f = (LogModel)ModelGenerater.GenerateFromJson(LogModel.class, json);
                    lst.add(f);
                }
            }
            logs = lst;
            return status;
        }

        @Override
        protected void overridePostExecute() {
            createViews();
        }
    }
    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
        }
        return false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_facility_logs, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        Refresh();
    }

    private void Refresh() {
        bundle = getArguments();
        int[] date = bundle.getIntArray("Date");
        int facility = bundle.getInt("Facility");

        LogRequest lr = new LogRequest();
        lr.Number = MainActivity.account.Number;
        lr.Token = MainActivity.account.Token;
        lr.Facility = facility;
        lr.Year = date[0];
        lr.Month = date[1];
        lr.Day = date[2];

        new LogGetTask().execute(lr);
    }

    private TextView createDpTextView(String text) {
        Resources r = getResources();
        int dp40 = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, r.getDisplayMetrics());

        TextView txtVoid = new TextView(getActivity());
        txtVoid.setWidth(dp40);
        txtVoid.setHeight(dp40);
        txtVoid.setText(text);

        return txtVoid;
    }

    private TextView createDpTextView(int width, int height, String text) {
        Resources r = getResources();
        int dp_width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, width, r.getDisplayMetrics());
        int dp_height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, height, r.getDisplayMetrics());

        TextView txtVoid = new TextView(getActivity());
        txtVoid.setWidth(dp_width);
        txtVoid.setHeight(dp_height);
        txtVoid.setText(text);

        return txtVoid;
    }

    private void createViews() {
        // 왜 getView()가 null을 return하는지 모르겠다.
        TableLayout layout = (TableLayout) getActivity().findViewById(R.id.log_table);
        layout.removeAllViews();

        // table header
        TableRow HeaderRow = new TableRow(getActivity());
        HeaderRow.addView(createDpTextView(30, 40, "#"));
        HeaderRow.addView(createDpTextView(80, 40, "Open"));
        HeaderRow.addView(createDpTextView(60, 40, "Name"));
        HeaderRow.addView(createDpTextView(100, 40, "Date"));
        HeaderRow.addView(createDpTextView(80, 40, "Close"));
        HeaderRow.addView(createDpTextView(60, 40, "Name"));
        HeaderRow.addView(createDpTextView(100, 40, "DateTime"));
        layout.addView(HeaderRow);

        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        int index = 1;
        for(LogModel model : logs) {
            TableRow row = new TableRow(getActivity());
            row.addView(createDpTextView(30, 40, String.valueOf(index++)));
            row.addView(createDpTextView(80, 40, model.OpenNumber));
            row.addView(createDpTextView(60, 40, model.OpenName));
            row.addView(createDpTextView(100, 40, f.format(model.OpenDate.getTime())));
            row.addView(createDpTextView(80, 40, model.CloseNumber));
            row.addView(createDpTextView(60, 40, model.CloseName));
            row.addView(createDpTextView(100, 40, f.format(model.CloseDate.getTime())));
            layout.addView(row);
        }
    }
}