package kr.ac.konyang.openky.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpResponse;

import java.util.ArrayList;

import kr.ac.konyang.openky.MainActivity;
import kr.ac.konyang.openky.R;
import kr.ac.konyang.openky.model.DecideApplicationModel;
import kr.ac.konyang.openky.model.RequestApplication;
import kr.ac.konyang.openky.requester.ApplicationRequester;

/**
 * Created by KyeongMin on 2015-05-20.
 */
public class ApplicationFragment extends BaseFragment implements Toolbar.OnMenuItemClickListener {

    public enum Mode {
        Create,
        Modify,
        Approval,
    }

    private class SubmitTask extends FragmentAsyncTask<RequestApplication, Void>
    {
        @Override
        protected Integer overrideDoInBackground(RequestApplication... params) {
            HttpResponse rp = ApplicationRequester.RequestSubmit(params[0]);
            if(rp == null)
                return 0;

            return rp.getStatusLine().getStatusCode();
        }

        @Override
        protected void overridePostExecute() {
            Toast.makeText(getActivity(), getString(R.string.status_submit), Toast.LENGTH_SHORT).show();
            startFragment(getFragmentManager(), ApplicationListFragment.class);
        }
    }

    private class RejectTask extends FragmentAsyncTask<DecideApplicationModel, Void>
    {
        @Override
        protected Integer overrideDoInBackground(DecideApplicationModel... params) {
            HttpResponse rp = ApplicationRequester.RequestReject(params[0]);
            if(rp == null)
                return 0;

            return rp.getStatusLine().getStatusCode();
        }

        @Override
        protected void overridePostExecute() {
            Toast.makeText(getActivity(), getString(R.string.status_submit), Toast.LENGTH_SHORT).show();
            startFragment(getFragmentManager(), ApplicationListFragment.class);
        }
    }

    private class AcceptTask extends FragmentAsyncTask<DecideApplicationModel, Void>
    {
        @Override
        protected Integer overrideDoInBackground(DecideApplicationModel... params) {
            HttpResponse rp = ApplicationRequester.RequestAccept(params[0]);
            if(rp == null)
                return 0;

            return rp.getStatusLine().getStatusCode();
        }

        @Override
        protected void overridePostExecute() {
            Toast.makeText(getActivity(), getString(R.string.status_submit), Toast.LENGTH_SHORT).show();
            startFragment(getFragmentManager(), ApplicationListFragment.class);
        }
    }

    private class CancelTask extends FragmentAsyncTask<DecideApplicationModel, Void>
    {
        @Override
        protected Integer overrideDoInBackground(DecideApplicationModel... params) {
            HttpResponse rp = ApplicationRequester.RequestCancel(params[0]);
            if(rp == null)
                return 0;

            return rp.getStatusLine().getStatusCode();
        }

        @Override
        protected void overridePostExecute() {
            Toast.makeText(getActivity(), getString(R.string.status_submit), Toast.LENGTH_SHORT).show();
            startFragment(getFragmentManager(), ApplicationListFragment.class);
        }
    }

    private Bundle bundle;
    private int year;
    private int month;
    private int day;
    private ArrayList<Integer> periods;

    private int Applicationid;

    private int buildingid;
    private String buildingname;

    private int facilityid;
    private String facilityname;

    private String purpose;
    private int peoples;

    private EditText editPeoples;
    private EditText editPurpose;

    private Mode mode;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // inflater를 이용해서 프레그먼트 레이아웃을 메모리에 올리고 리턴함.
        Bundle bundle = getArguments();
        String modeStr = bundle.getString("Mode");
        if(modeStr == null || modeStr.equals(Mode.Create.toString()))
            mode = Mode.Create;
        else if(modeStr.equals(Mode.Modify.toString()))
            mode = Mode.Modify;
        else if(modeStr.equals(Mode.Approval.toString()))
            mode = Mode.Approval;

        // bundle.remove("Mode");

        Applicationid = bundle.getInt("ApplicationId");
        periods = bundle.getIntegerArrayList("Periods");

        year = bundle.getInt("Year");
        month = bundle.getInt("Month");
        day = bundle.getInt("Day");

        buildingid = bundle.getInt("Building");
        buildingname = bundle.getString("BuildingName");

        facilityname = bundle.getString("FacilityName");
        facilityid = bundle.getInt("Facility");

        purpose = bundle.getString("Purpose");
        peoples = bundle.getInt("Peoples");

        Toolbar t = (Toolbar)getActivity().findViewById(R.id.toolbar);
        if(t != null)
        {
            switch(mode)
            {
                case Create:
                    t.inflateMenu(R.menu.application);
                    break;

                case Modify:
                    t.inflateMenu(R.menu.application_modify);
                    break;

                case Approval:
                    t.inflateMenu(R.menu.application_approval);
                    break;
            }
            t.setOnMenuItemClickListener(this);
        }
        return inflater.inflate(R.layout.fragment_application, null);
    }

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch(menuItem.getItemId())
        {
            case R.id.action_submit:
                try {
                    RequestApplication submit = new RequestApplication();
                    submit.Year = year;
                    submit.Month = month;
                    submit.Day = day;
                    submit.Facility = facilityid;
                    String str = "";
                    for (int i = 0; i < periods.size(); i++) {
                        str += periods.get(i);
                        if (i + 1 < periods.size())
                            str += ",";
                    }
                    submit.Periods = str;
                    submit.Purpose = editPurpose.getText().toString();
                    submit.Peoples = Integer.parseInt(editPeoples.getText().toString());
                    submit.Number = MainActivity.account.Number;
                    submit.Token = MainActivity.account.Token;

                    new SubmitTask().execute(submit);
                } catch (NumberFormatException e) {
                    Toast.makeText(getActivity(), getString(R.string.error_empty_peoples), Toast.LENGTH_SHORT).show();
                }

                InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(0, 0);
                break;

            case R.id.action_accept:
                DecideApplicationModel accept = new DecideApplicationModel();
                accept.Number = MainActivity.account.Number;
                accept.Token = MainActivity.account.Token;
                accept.Id = Applicationid;
                new AcceptTask().execute(accept);
                break;

            case R.id.action_reject:
                DecideApplicationModel reject = new DecideApplicationModel();
                reject.Number = MainActivity.account.Number;
                reject.Token = MainActivity.account.Token;
                reject.Id = Applicationid;
                new RejectTask().execute(reject);
                break;

            case R.id.action_cancel:
                DecideApplicationModel cancel = new DecideApplicationModel();
                cancel.Number = MainActivity.account.Number;
                cancel.Token = MainActivity.account.Token;
                cancel.Id = Applicationid;
                new CancelTask().execute(cancel);
                break;
        }
        return false;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        EditText editDate = (EditText)getView().findViewById(R.id.txtApplicationDate);
        editDate.setText(year+"/"+(month)+"/"+day);

        EditText editPeriod = (EditText)getView().findViewById(R.id.txtPeriod);
        String str = "";
        for(int i = 0; i<periods.size(); i++)
        {
            str += periods.get(i);
            if(i+1 < periods.size())
                str += ", ";
        }
        editPeriod.setText(str);

        EditText editNumber = (EditText)getView().findViewById(R.id.txtNumber);
        editNumber.setText(MainActivity.account.Number);

        EditText editFacility = (EditText)getView().findViewById(R.id.txtFacility);
        editFacility.setText("["+buildingname+"] "+facilityname);

        editPeoples = (EditText)getView().findViewById(R.id.txtPeoples);
        editPurpose = (EditText)getView().findViewById(R.id.txtPurpose);

        if(mode == Mode.Approval || mode == Mode.Modify) {
            EditText editPeople = (EditText)getView().findViewById(R.id.txtPeoples);
            EditText editPurpose = (EditText)getView().findViewById(R.id.txtPurpose);

            editPeople.setText(String.valueOf(peoples));
            editPurpose.setText(purpose);

            editPeople.setKeyListener(null);
            editPurpose.setKeyListener(null);
        }
    }
}
