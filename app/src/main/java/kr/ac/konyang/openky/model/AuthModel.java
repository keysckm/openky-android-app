package kr.ac.konyang.openky.model;

/**
 * Created by KyeongMin on 2015-05-22.
 */
public class AuthModel {
    public String Number;
    public String Token;

    public AuthModel() {

    }

    public AuthModel(String number, String token) {
        Number = number;
        Token = token;
    }
}
