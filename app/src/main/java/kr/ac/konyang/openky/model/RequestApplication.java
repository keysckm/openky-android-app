package kr.ac.konyang.openky.model;

/**
 * Created by KyeongMin on 2015-05-22.
 */
public class RequestApplication extends AuthModel {
    public int Facility;
    public int Year;
    public int Month;
    public int Day;
    public String Periods;
    public String Purpose;
    public int Peoples;
}

