package kr.ac.konyang.openky.requester;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import kr.ac.konyang.openky.MainActivity;

/**
 * Created by KyeongMin on 2015-05-20.
 */
public class BuildingRequester {

    public static final String urlString = "api/Building";

    public static HttpResponse RequestBuildings()
    {
        HttpClient httpClient = new DefaultHttpClient();
        try{
            URI url = new URI(MainActivity.baseUrl + urlString);
            HttpGet get = new HttpGet();
            get.setURI(url);

            return httpClient.execute(get);

        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
