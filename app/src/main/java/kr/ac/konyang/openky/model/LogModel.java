package kr.ac.konyang.openky.model;

import java.util.Calendar;

/**
 * Created by KyeongMin on 2015-06-13.
 */
public class LogModel {
    public String Facility;
    public String OpenNumber;
    public String OpenName;
    public Calendar OpenDate;
    public String CloseNumber;
    public String CloseName;
    public Calendar CloseDate;
}
