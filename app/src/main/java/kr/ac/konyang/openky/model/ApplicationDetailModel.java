package kr.ac.konyang.openky.model;

import java.util.Calendar;

public class ApplicationDetailModel {
    public int Id;                      // 신청서 인덱스
    public String Building;             // 시설물이 속한 건물 명칭
    public String Facility;             // 시설물 명칭
    public int Year;                    // 신청 년도
    public int Month;                   // 신청 월
    public int Day;                     // 신청 일
    public String Period;               // 신청 교시
    public int Peoples;                 // 신청 인원
    public String Purpose;              // 신청 목적
    public int Approval;                // 승인권자 승인
    public Calendar ApplicationDate;    // 신청서 제출 시간

    /**
     * Approval1, 2의 값에 따른 분류
     * 0: 아직 정의되지 않음
     * 1: 승인됨
     * 2: 거절됨
     */
}

