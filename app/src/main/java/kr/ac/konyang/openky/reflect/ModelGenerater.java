package kr.ac.konyang.openky.reflect;

import android.os.Bundle;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

/**
 * Created by KyeongMin on 2015-05-24.
 * 리플렉션을 이용한 모델 생성 클래스
 */
public class ModelGenerater {

    public static Object GenerateFromJson(Class<?> cls, JSONObject obj) {

        // JSON 데이터의 Key목록을 가져옴.
        Iterator<String> iter = obj.keys();
        // Model 객체 생성
        Object info = null;
        try {
            info = cls.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        if(info == null)
            throw new IllegalStateException("cannot create instance model");

        while(iter.hasNext()) {
            // 키 문자열 가져옴
            String key = iter.next();
            Field f = null;
            // 모델 메타데이터로부터 필드 정보를 찾음
            try {
                f = cls.getDeclaredField(key);
            } catch(NoSuchFieldException e) {
                e.printStackTrace();
                continue;   // 만약 모델에 해당 필드가 없다면 이번 JSON 데이터는 무시한다.
            }

            Class<?> type = f.getType();    // 필드의 타입을 가져옴
            try {
                if (type == int.class) {
                    f.setInt(info, obj.getInt(f.getName()));
                } else if (type == String.class) {
                    f.set(info, obj.getString(f.getName()));
                } else if (type == Boolean.class) {     // Boolean의 경우, true, false, null값이 있을 수 있음
                    try {                               // null인 경우 JSONException이 발생함.
                        f.setBoolean(info, obj.getBoolean(f.getName()));
                    } catch (JSONException e) { f.set(info, null); }
                } else if(type == double.class) {
                    f.setDouble(info, obj.getDouble(f.getName()));
                } else if(type == float.class) {
                    f.setFloat(info, (float)obj.getDouble(f.getName()));
                } else if (type == Calendar.class) {        // 서버에서는 항상 yyyy-MM-ddHH:mm:ss포멧 스트링으로 날짜데이터를 보냄
                    Calendar cal = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-ddHH:mm:ss");
                    cal.setTime(sdf.parse(obj.getString(key)));
                    f.set(info, cal);
                }  else if (type == List.class) {             // 리스트 객체인 경우
                    // JSONArray객체로 받아옴
                    JSONArray arry = obj.getJSONArray(f.getName());
                    // 해당 List Collection의 Generic Type을 조사함.
                    ParameterizedType pt = (ParameterizedType) f.getGenericType();
                    Class<?> generic_type = (Class<?>) pt.getActualTypeArguments()[0];
                    List<?> lst = listof(generic_type);

                    for (int i = 0; i < arry.length(); i++) {   // 조사한 Generic Type별로 처리함. 위와 같음.
                        if (generic_type == Integer.class) {
                            ((List<Integer>)lst).add(arry.getInt(i));
                        } else if (generic_type == String.class) {
                            ((List<String>)lst).add(arry.getString(i));
                        } else if (generic_type == Boolean.class) {
                            try {
                                ((List<Boolean>) lst).add(arry.getBoolean(i));
                            } catch (JSONException e) {
                                ((List<Boolean>) lst).add((Boolean) null);
                            }
                        } else if (generic_type == Calendar.class) {
                            Calendar cal = Calendar.getInstance();
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-ddHH:mm:ss");
                            cal.setTime(sdf.parse(arry.getString(i)));
                            ((List<Calendar>)lst).add(cal);
                        } else {        // 특정 모델 오브젝트인 경우 재귀함.
                            ((List<Object>)lst).add(GenerateFromJson(generic_type, arry.getJSONObject(i)));
                        }
                    }
                    f.set(info, lst);
                }
            } catch(JSONException e) {
                e.printStackTrace();
                continue;
            } catch (ParseException e) {
                e.printStackTrace();
                continue;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                continue;
            }
        }

        // 생성된 객체를 리턴
        return info;
    }

    // 파악된 리스트의 제네릭 타입에 맞는 새로운 리스트를 인스턴스화 하여 리턴하기 위한 서포트 메서드.
    private static <T> List<T> listof(Class<T> cls)
    {
        return new ArrayList<T>();
    }
}
