package kr.ac.konyang.openky;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by KyeongMin on 2015-06-06.
 */
public class FacilityStateItemView extends LinearLayout {

    private TextView txtBuildingName;
    private TextView txtFacilityName;
    private TextView txtState;

    public FacilityStateItemView(Context context) {
        super(context);

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.listitem_facility_status, this, true);

        txtBuildingName = (TextView)findViewById(R.id.txtBuildingName);
        txtFacilityName = (TextView)findViewById(R.id.txtFacilityName);
        txtState = (TextView)findViewById(R.id.txtFacilityState);
    }

    public FacilityStateItemView setBuildingName(String name)
    {
        txtBuildingName.setText(name);
        return this;
    }

    public FacilityStateItemView setFacilityName(String name)
    {
        txtFacilityName.setText(name);
        return this;
    }

    public FacilityStateItemView setState(String state)
    {
        txtState.setText(state);
        return this;
    }
}
