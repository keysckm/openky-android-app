package kr.ac.konyang.openky;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

/**
 * Created by KyeongMin on 2015-05-19.
 */
public class NavigationListAdapter extends BaseAdapter {

    private Context context;
    private List<NavigationItem> items;
    private UserDataItem userdata;

    public NavigationListAdapter(Context context, List<NavigationItem> items)
    {
        this.context = context;
        this.items = items;
    }

    public NavigationListAdapter setUserDataItem(UserDataItem item)
    {
        userdata = item;
        return this;
    }

    @Override
    public int getCount() {
        return items.size()+1;
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position)
    {
        return position == 0 ? 0 : 1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        int type = getItemViewType(position);

        if(convertView == null) {
            if (type == 0)
                convertView = new NavigationUserdataView(context);
            else
                convertView = new NavigationItemView(context);
        }

        if(type == 0) {
            NavigationUserdataView viewitem = (NavigationUserdataView) convertView;

            if(userdata.icon != null)
                viewitem.setIcon(userdata.icon);

            viewitem.setName(userdata.name);
            viewitem.setType(userdata.type);
            viewitem.setDepartment(userdata.department);


            return viewitem;
        }
        else {
            NavigationItemView viewitem = (NavigationItemView) convertView;
            NavigationItem item = items.get(position-1);

            if (item.icon != null)
                viewitem.setIcon(item.icon);

            viewitem.setTitle(item.title);
            viewitem.setExecute(item.fragment);

            return viewitem;
        }
    }
}
