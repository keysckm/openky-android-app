package kr.ac.konyang.openky.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import org.apache.http.HttpResponse;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import kr.ac.konyang.openky.FacilityItemView;
import kr.ac.konyang.openky.R;
import kr.ac.konyang.openky.model.FacilityModel;
import kr.ac.konyang.openky.reflect.ModelGenerater;
import kr.ac.konyang.openky.requester.FacilityRequester;

/**
 * Created by KyeongMin on 2015-05-20.
 */
public class FacilitiesFragment extends BaseFragment implements AdapterView.OnItemClickListener {

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        bundle.putInt("Facility", (int) id);
        bundle.putString("FacilityName", lstItems.get(position).Name);
        super.bundle = bundle;
        startFragment(getFragmentManager(), FacilitySchedulesFragment.class);
    }

    // Adapters
    public class FacilitiesAdapter extends BaseAdapter {
        private Context context;
        private List<FacilityModel> items;

        public FacilitiesAdapter(Context context, List<FacilityModel> items) {
            this.context = context;
            this.items = items;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return items.get(position).Index;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            FacilityItemView v;
            if (convertView == null)
                v = new FacilityItemView(context);
            else
                v = (FacilityItemView) convertView;

            v.setName(items.get(position).Name);
            return v;
        }
    }

    private class FacilityTask extends FragmentAsyncTask<Integer, Void> {

        @Override
        protected Integer overrideDoInBackground(Integer... params) throws IOException, JSONException, NullPointerException{

            int status = 0;
            HttpResponse rp = FacilityRequester.RequestFacility(params[0]);
            ArrayList<FacilityModel> lst = new ArrayList<FacilityModel>();
            status = rp.getStatusLine().getStatusCode();
            if (status == 200) {
                String response_body = EntityUtils.toString(rp.getEntity(), HTTP.UTF_8);
                Log.d("request", response_body + "code: " + rp.getStatusLine().getStatusCode());

                JSONArray arry = new JSONArray(response_body);
                for (int i = 0; i < arry.length(); i++) {
                    JSONObject json = arry.getJSONObject(i);
                    FacilityModel f = (FacilityModel)ModelGenerater.GenerateFromJson(FacilityModel.class, json);
                    lst.add(f);
                }
            }
            lstItems = lst;
            return status;
        }

        @Override
        protected void overridePostExecute() {
            lvSimpleStringView.setAdapter(new FacilitiesAdapter(getView().getContext(), lstItems));
        }
    }

    private ListView lvSimpleStringView;
    private ArrayList<FacilityModel> lstItems;
    private Bundle bundle;
    private int parentBuildingIndex;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_facilities, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        bundle = getArguments();
        parentBuildingIndex = bundle.getInt("Building");
        lvSimpleStringView = (ListView) getView().findViewById(R.id.lvFacilities);
        lvSimpleStringView.setEmptyView(getView().findViewById(R.id.emptyContent));
        lvSimpleStringView.setOnItemClickListener(this);

        new FacilityTask().execute(parentBuildingIndex);
    }
}
