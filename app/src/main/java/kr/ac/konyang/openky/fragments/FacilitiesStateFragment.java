package kr.ac.konyang.openky.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import kr.ac.konyang.openky.FacilityStateItemView;
import kr.ac.konyang.openky.MainActivity;
import kr.ac.konyang.openky.R;
import kr.ac.konyang.openky.model.AuthModel;
import kr.ac.konyang.openky.model.FacilityStateModel;
import kr.ac.konyang.openky.reflect.ModelGenerater;
import kr.ac.konyang.openky.requester.FacilityRequester;

/**
 * Created by KyeongMin on 2015-06-06.
 */
public class FacilitiesStateFragment extends BaseFragment implements ListView.OnItemClickListener, DatePickerDialog.OnDateSetListener {

    private boolean isCancel = false;

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        if(isCancel == true)
            return;

        bundle.putIntArray("Date", new int[]{year, monthOfYear+1, dayOfMonth});
        startFragment(getFragmentManager(), FacilityLogFragment.class);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        isCancel = false;

        if(bundle == null)
            bundle = new Bundle();
        bundle.putInt("Facility", lstItems.get(position).Index);

        Calendar c = Calendar.getInstance();

        DatePickerDialog dpd = new DatePickerDialog(getView().getContext(), this, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        dpd.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.btn_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(getActivity(), getString(R.string.status_cancel), Toast.LENGTH_SHORT).show();
                isCancel = true;
                dialog.dismiss();
            }
        });
        dpd.show();
    }

    class RefreshTask extends FragmentAsyncTask<AuthModel, Void>
    {
        @Override
        protected Integer overrideDoInBackground(AuthModel... params) throws IOException, JSONException, NullPointerException {
            int status = 0;
            HttpResponse rp = FacilityRequester.RequestState(params[0]);
            status = rp.getStatusLine().getStatusCode();
            ArrayList<FacilityStateModel> lst = new ArrayList<FacilityStateModel>();
            if(rp.getStatusLine().getStatusCode() == 200)
            {
                String response_body = null;
                response_body = EntityUtils.toString(rp.getEntity(), HTTP.UTF_8);
                Log.d("request", response_body + "code: " + rp.getStatusLine().getStatusCode());

                JSONArray arry = new JSONArray(response_body);
                for(int i = 0; i<arry.length(); i++) {
                    JSONObject json = arry.getJSONObject(i);
                    FacilityStateModel state = (FacilityStateModel) ModelGenerater.GenerateFromJson(FacilityStateModel.class, json);
                    lst.add(state);
                }
            }
            lstItems = lst;
            return status;
        }

        @Override
        protected void overridePostExecute() {
            lvItems.setAdapter(new StateAdapter(getView().getContext(), lstItems));
        }
    }

    // Adapters
    public class StateAdapter extends BaseAdapter {
        private Context context;
        private List<FacilityStateModel> items;

        public StateAdapter(Context context, List<FacilityStateModel> items) {
            this.context = context;
            this.items = items;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return items.get(position).Index;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            FacilityStateItemView v;
            if (convertView == null)
                v = new FacilityStateItemView(context);
            else
                v = (FacilityStateItemView) convertView;

            FacilityStateModel model = items.get(position);
            v.setBuildingName(model.Building)
                    .setFacilityName(model.Name)
                    .setState(model.State == 0 ? "Unknown" : model.State == 1 ? "열림" : "닫힘");

            return v;
        }
    }

    private ArrayList<FacilityStateModel> lstItems;
    private ListView lvItems;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_facilities, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        lvItems = (ListView)getView().findViewById(R.id.lvFacilities);
        lvItems.setEmptyView(getView().findViewById(R.id.emptyContent));
        lvItems.setOnItemClickListener(this);
        new RefreshTask().execute(new AuthModel(MainActivity.account.Number, MainActivity.account.Token));
    }
}
