package kr.ac.konyang.openky;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by KyeongMin on 2015-05-19.
 */
public class NavigationUserdataView extends LinearLayout {

    private ImageView ivIcon;
    private TextView txtName;
    private TextView txtType;
    private TextView txtDepartment;

    public NavigationUserdataView(Context context)
    {
        super(context);

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.userdata_navigation, this, true);

        ivIcon = (ImageView)findViewById(R.id.iconItem);
        txtName = (TextView)findViewById(R.id.txtName);
        txtType = (TextView)findViewById(R.id.txtType);
        txtDepartment = (TextView)findViewById(R.id.txtDepartment);
    }

    public void setIcon(Drawable icon)
    {
        ivIcon.setImageDrawable(icon);
    }

    public void setName(String name)
    {
        txtName.setText(name);
    }

    public void setType(String type)
    {
        txtType.setText(type);
    }

    public void setDepartment(String department)
    {
        txtDepartment.setText(department);
    }
}
