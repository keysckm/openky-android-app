package kr.ac.konyang.openky.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import org.apache.http.HttpResponse;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import kr.ac.konyang.openky.MainActivity;
import kr.ac.konyang.openky.MyApplicationItemView;
import kr.ac.konyang.openky.R;
import kr.ac.konyang.openky.model.AuthModel;
import kr.ac.konyang.openky.model.ApplicationDetailModel;
import kr.ac.konyang.openky.reflect.ModelGenerater;
import kr.ac.konyang.openky.requester.ApplicationRequester;

/**
 * Created by KyeongMin on 2015-05-23.
 *
 */
public class ApplicationListFragment extends BaseFragment implements Toolbar.OnMenuItemClickListener, AdapterView.OnItemClickListener {

    private ListView lvItems;
    private List<ApplicationDetailModel> items;

    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch(menuItem.getItemId())
        {
            case R.id.action_refresh:
                refresh();
                break;
        }

        return false;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(bundle == null)
            bundle = new Bundle();

        switch(MainActivity.account.Type) {
            case 1:
                bundle.putString("Mode", ApplicationFragment.Mode.Modify.toString());
                break;

            case 3:
                bundle.putString("Mode", ApplicationFragment.Mode.Approval.toString());
                break;
        }

        ApplicationDetailModel application = items.get(position);

        String[] strperiods = application.Period.split(",");
        ArrayList<Integer> periods = new ArrayList<Integer>();
        for(String period : strperiods)
            periods.add(Integer.parseInt(period));

        bundle.putIntegerArrayList("Periods", periods);
        bundle.putInt("Year", application.Year);
        bundle.putInt("Month", application.Month);
        bundle.putInt("Day", application.Day);

        bundle.putString("BuildingName", application.Building);
        bundle.putString("FacilityName", application.Facility);
        bundle.putInt("ApplicationId", application.Id);
        bundle.putString("Purpose", application.Purpose);
        bundle.putInt("Peoples", application.Peoples);

        startFragment(getFragmentManager(), ApplicationFragment.class);
    }

    // Task
    public class ApplicationListTask extends FragmentAsyncTask<AuthModel, Void> {

        @Override
        protected Integer overrideDoInBackground(AuthModel... params) throws IOException, JSONException, NullPointerException {

            List<ApplicationDetailModel> lst = new ArrayList<ApplicationDetailModel>();
            int status = 0;
            HttpResponse rp = ApplicationRequester.RequestApplications(params[0]);
            if(rp == null)
                return status;

            status = rp.getStatusLine().getStatusCode();

            if(status == 200) {
                String response_body = EntityUtils.toString(rp.getEntity(), HTTP.UTF_8);
                JSONArray arry = new JSONArray(response_body);

                for (int i = 0; i < arry.length(); i++) {
                    JSONObject obj = arry.getJSONObject(i);
                    ApplicationDetailModel model = (ApplicationDetailModel) ModelGenerater.GenerateFromJson(ApplicationDetailModel.class, obj);
                    lst.add(model);
                }
            }

            items = lst;
            return status;
        }

        @Override
        public void overridePostExecute() {
            lvItems.setAdapter(new ApplicationListAdapter(getView().getContext(), items));
        }
    }

    // Adapter
    public class ApplicationListAdapter extends BaseAdapter {

        private Context context;
        private List<ApplicationDetailModel> items;

        public ApplicationListAdapter(Context context, List<ApplicationDetailModel> items) {
            this.context = context;
            this.items = items;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return items.get(position).Id;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            MyApplicationItemView v;
            if(convertView == null)
                v = new MyApplicationItemView(context);
            else
                v = (MyApplicationItemView)convertView;

            v.setContent(items.get(position));

            return v;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list_applications, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Toolbar t = (Toolbar)getActivity().findViewById(R.id.toolbar);
        if(t != null)
        {
            t.inflateMenu(R.menu.refresh);
            t.setOnMenuItemClickListener(this);
        }

        lvItems = (ListView)getView().findViewById(R.id.lvApplications);
        lvItems.setEmptyView(getView().findViewById(R.id.emptyContent));
        lvItems.setOnItemClickListener(this);
        refresh();
    }

    private void refresh()
    {
        // 접속자가 학생이면 학생의 신청서, 교직원이면 담당 시설물에 대한 신청서를 가져온다.
        // 교직원의 경우 아직 가불결정이 되지 않은 신청서만 가져옴.
        items = null;
        new ApplicationListTask().execute(new AuthModel(MainActivity.account.Number, MainActivity.account.Token));
    }
}
