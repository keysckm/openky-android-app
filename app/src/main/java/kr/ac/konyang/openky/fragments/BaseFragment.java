package kr.ac.konyang.openky.fragments;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.TableLayout;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import kr.ac.konyang.openky.R;

/**
 * Created by KyeongMin on 2015-05-20.
 * 모든 Fragment의 Base가 되는 클래스입니다.
 * Fragment간 전환에 대한 처리를 수행합니다.
 */
public class BaseFragment extends Fragment {

    // progress dialog
    protected ProgressDialog pdlg;

    // 다른 fragment로 전환할 때 넘겨줄 bundle 데이터
    protected Bundle bundle = null;

    /**
     * Toolbar Action Menu의 Clear관련 Flag입니다.
     * True라면 Fragment Switching이 발생할 때 Action Menu가 Clear됩니다.
     */
    protected boolean isMenuClear = true;

    @Override
    public void onDestroyView() {

        if(isMenuClear) {
            Toolbar t = (Toolbar) getActivity().findViewById(R.id.toolbar);
            if (t != null) {
                t.getMenu().clear();                // 메뉴 클리어
                t.setOnMenuItemClickListener(null); // 리스너 제거
            }
        }

        super.onDestroyView();
    }

    /**
     * fragment를 전환합니다.
     * @param fm Base Activity의 Fragment Manager입니다.
     * @param fragmentClass 전환될 fragment의 class입니다.
     */
    protected void startFragment(FragmentManager fm, Class<? extends BaseFragment> fragmentClass) {
        BaseFragment fragment = null;

        try {
            fragment = fragmentClass.newInstance(); // 넘겨받은 Class 데이터를 이용해 리플렉션하여 Instance를 생성
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        if (fragment == null) {
            throw new IllegalStateException("cannot start fragment. " + fragmentClass.getName());
        }

        if(bundle != null)  // 만약 bundle에 데이터가 있다면 fragment에 argument로 세팅
            fragment.setArguments(bundle);

        // TODO: 현재 backstack을 clear하면 현재 fragment의 이전 fragment가 부활하면서 getView()같은 부모 Activity 객체를 참조하는 코드가 실행 될 경우
        // TODO: null을 리턴하는 문제가 있음. 백스택에서 제거되면서 Activity의 FragmentManager로부터 삭제되었기 때문에 부모 Activity가 없어서 발생하는 문제인듯 함. 해결법 찾는중.

        // 인자로 받은 fragment manager를 이용해서 fragment를 전환함
        fm.beginTransaction().replace(R.id.container, fragment).addToBackStack(null).commit();
        Log.d("fragment", "back stack count: " + fm.getBackStackEntryCount());
    }

    protected void finishFragment() {
        getFragmentManager().popBackStack();
    }

    protected LayoutInflater getLayoutInflater() {
        return (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    protected void showProgressDialog() {
        pdlg = ProgressDialog.show(getActivity(), "", getString(R.string.status_refresh), true);
    }

    protected void hideProgressDialog() {
        if(pdlg != null)
            pdlg.dismiss();

        pdlg = null;
    }

    public abstract class FragmentAsyncTask<Params, Progress> extends AsyncTask<Params, Progress, Integer> {
        @Override
        public final void onPreExecute() {
            overridePreExecute();
            showProgressDialog();
        }

        /**
         * FragmentAsyncTask는 onPreExecute메소드 대신 overridePreExecute 메소드를 상속하여야 합니다.
         */
        protected void overridePreExecute() {

        }

        @Override
        protected final Integer doInBackground(Params... p) {
            Integer result = 0;
            try {
                result = overrideDoInBackground(p);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
            return result;
        }

        /**
         * FragmentAsyncTask는 doInBackground메소드 대신 overrideDoInBackground메소드를 상속하여야 합니다.
         * @param p HTTP요청에 필요한 Model클래스입니다.
         * @return HTTP요청의 결과입니다.
         * @throws IOException
         * @throws JSONException
         * @throws NullPointerException
         */
        protected abstract Integer overrideDoInBackground(Params... p) throws IOException, JSONException, NullPointerException;

        @Override
        public final void onPostExecute(Integer result) {
            switch(result)
            {
                case 200:
                    overridePostExecute();
                    break;

                case 404:   // Not Found
                    Toast.makeText(getActivity(), getString(R.string.status_not_found), Toast.LENGTH_SHORT).show();
                    break;

                case 403:   // 인증 무효
                    Toast.makeText(getActivity(), getString(R.string.status_wrong_token), Toast.LENGTH_SHORT).show();
                    break;

                case 400:   // bad request
                    Toast.makeText(getActivity(), getString(R.string.status_bad_request), Toast.LENGTH_SHORT).show();
                    break;

                case 500:
                    Toast.makeText(getActivity(), getString(R.string.status_server_error), Toast.LENGTH_SHORT).show();
                    break;

                case 0:
                    Toast.makeText(getActivity(), getString(R.string.status_server_shutdown), Toast.LENGTH_SHORT).show();
                    break;
            }
            hideProgressDialog();
        }

        /**
         * FragmentAsyncTask는 onPostExecute메소드 대신 overridePostExecute 메소드를 상속하여야 합니다.
         * result가 200인 경우에만 동작합니다.
         */
        protected void overridePostExecute() {

        }
    }
}