package kr.ac.konyang.openky.requester;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import kr.ac.konyang.openky.MainActivity;
import kr.ac.konyang.openky.reflect.NvprGenerater;

/**
 * Created by KyeongMin on 2015-06-17.
 */
public class LogRequester extends BaseRequester {

    public static final String urlString = "api/Log";

    public static HttpResponse RequestLog(kr.ac.konyang.openky.model.LogRequest model) {
        HttpClient httpClient = new DefaultHttpClient();
        try {
            URI url = new URI(MainActivity.baseUrl + urlString);
            HttpCustomPost method = new HttpCustomPost();
            method.setMethod("GET");
            List<NameValuePair> nvpr = NvprGenerater.generate(model);
            method.setURI(url);
            method.setEntity(new UrlEncodedFormEntity(nvpr, "utf-8"));

            return httpClient.execute(method);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
