package kr.ac.konyang.openky.model;

/**
 * Created by KyeongMin on 2015-05-19.
 */
public class Account {
    public String Number;
    public String Name;
    public int Type;
    public String Department;
    public String Token;

    /**
     * Type
     * 1: 학생
     * 2: 교수
     * 3: 교직원
     */
}
