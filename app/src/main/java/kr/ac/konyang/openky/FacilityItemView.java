package kr.ac.konyang.openky;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by KyeongMin on 2015-05-20.
 */
public class FacilityItemView extends LinearLayout {

    private TextView txtName;

    public FacilityItemView(Context context) {
        super(context);

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.listitem_facilitylist, this, true);

        txtName = (TextView)findViewById(R.id.txtFacility);
    }

    public void setName(String name)
    {
        txtName.setText(name);
    }
}
