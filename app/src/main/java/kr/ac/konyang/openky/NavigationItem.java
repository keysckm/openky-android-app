package kr.ac.konyang.openky;

import android.graphics.drawable.Drawable;

import kr.ac.konyang.openky.fragments.BaseFragment;

/**
 * Created by KyeongMin on 2015-05-19.
 */
public class NavigationItem {
    public Drawable icon;
    public String title;
    public Class<? extends BaseFragment> fragment;

    public NavigationItem(Drawable icon, String title, Class<? extends BaseFragment> fragment)
    {
        this.icon = icon;
        this.title = title;
        this.fragment = fragment;
    }
}
