package kr.ac.konyang.openky.model;

import java.util.List;

/**
 * Created by KyeongMin on 2015-05-21.
 */
public class ScheduleModel {
    public int Year;
    public int Month;
    public int Day;
    public List<Integer> Period;
}
