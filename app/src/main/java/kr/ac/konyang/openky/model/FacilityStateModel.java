package kr.ac.konyang.openky.model;

/**
 * Created by KyeongMin on 2015-06-06.
 */
public class FacilityStateModel {
    public int Index;
    public String Building;
    public String Name;
    public int State;

    /**
     * state:
     * 0: unknown
     * 1: Open
     * 2: Close
     */
}
