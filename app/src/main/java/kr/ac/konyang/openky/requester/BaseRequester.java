package kr.ac.konyang.openky.requester;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;

/**
 * Created by KyeongMin on 2015-06-17.
 */
public class BaseRequester {
    protected static class HttpCustomPost extends HttpPost {
        private String __method;

        public void setMethod(String method) { __method = method; }

        @Override
        public String getMethod() { return __method; }
    }
}
