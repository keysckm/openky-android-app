package kr.ac.konyang.openky;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import org.apache.http.HttpResponse;

import java.util.ArrayList;
import java.util.List;

import kr.ac.konyang.openky.fragments.BaseFragment;
import kr.ac.konyang.openky.fragments.BuildingsFragment;
import kr.ac.konyang.openky.fragments.ApplicationListFragment;
import kr.ac.konyang.openky.fragments.FacilitiesStateFragment;
import kr.ac.konyang.openky.fragments.FacilityLogFragment;
import kr.ac.konyang.openky.requester.AccountRequest;


public class ContentActivity extends ActionBarActivity
{
    private ContentActivity self;

    private List<NavigationItem> items;
    private ListView lvNavigation;
    private FrameLayout frmContent;

    // navigation bar toggle button
    private DrawerLayout dlDrawer;
    private ActionBarDrawerToggle dtToggle;
    private Toolbar toolbar;

    // 마지막으로 backkey를 누른 시각
    private long mBackkeyPushTime = 0;

    private void startFragment(Class<? extends BaseFragment> fragmentClass, @Nullable Bundle bundle)
    {
        BaseFragment fragment = null;

        try {
            fragment = fragmentClass.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        if(fragment == null) {
            throw new IllegalStateException("cannot start fragment. " + fragmentClass.getName());
        }

        if(bundle != null)
            fragment.setArguments(bundle);

        FragmentManager fm = getFragmentManager();
        fm.beginTransaction().replace(R.id.container, fragment).commit();
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener
    {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if(position == 0) {
                NavigationUserdataView v = (NavigationUserdataView)view;
            } else {
                NavigationItemView v = (NavigationItemView)view;
                String title = v.getTitle();

                // 로그아웃인 경우에는 LogoutTask를 Execute한다.
                if(title.equals(getString(R.string.navigation_logout))) {
                    new LogoutTask().execute(MainActivity.account.Token);
                } else {    // 그 외의 경우에는 ItemView로부터 Class를 받아 실행한다.
                    Class<? extends BaseFragment> frg = v.getExecute();
                    if(frg == null)
                        Toast.makeText(getApplicationContext(), getString(R.string.toast_not_define_action), Toast.LENGTH_SHORT).show();
                    else
                        startFragment(v.getExecute(), null);
                }

                dlDrawer.closeDrawers();
            }
        }
    }

    private class LogoutTask extends AsyncTask<String, Void, Integer>
    {
        @Override
        protected Integer doInBackground(String... params) {
            String token = params[0];
            HttpResponse rp = AccountRequest.RequestLogout(token);

            return rp.getStatusLine().getStatusCode();
        }

        @Override
        protected void onPostExecute(Integer result)
        {
            String message;
            switch(result)
            {
                case 200:
                    message = getString(R.string.status_logout_success);
                    break;

                case 404:
                    message = getString(R.string.status_wrong_token);
                    break;

                case 500:
                    message = getString(R.string.status_server_error);
                    break;

                default:
                    message = getString(R.string.status_unknown_error);
                    break;
            }
            Toast.makeText(getApplicationContext(), message,Toast.LENGTH_SHORT).show();
            MainActivity.account = null;
            MainActivity.config.writeConfig("Token", "");
            Intent intent = new Intent(ContentActivity.this, MainActivity.class);
            startActivity(intent);
            self.finish();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);
        this.setTitle(getString(R.string.app_name));

        self = this;

        lvNavigation = (ListView)findViewById(R.id.lv_activity_main_nav_list);
        frmContent = (FrameLayout)findViewById(R.id.container);

        Resources res = getResources();
        items = new ArrayList<NavigationItem>();
        if(MainActivity.account.Type == 1) {
            items.add(new NavigationItem(res.getDrawable(R.drawable.ic_add), getString(R.string.navigation_request_application), BuildingsFragment.class));
            items.add(new NavigationItem(res.getDrawable(R.drawable.ic_applications), getString(R.string.navigation_my_application), ApplicationListFragment.class));
        }
        else if(MainActivity.account.Type == 3) {
            items.add(new NavigationItem(res.getDrawable(R.drawable.ic_applications), getString(R.string.navigation_confirm_application), ApplicationListFragment.class));
            items.add(new NavigationItem(res.getDrawable(R.drawable.ic_lock), getString(R.string.navigation_check_facility), FacilitiesStateFragment.class));
        }
        items.add(new NavigationItem(res.getDrawable(R.drawable.ic_modify), getString(R.string.navigation_setup), null));
        items.add(new NavigationItem(res.getDrawable(R.drawable.ic_key), getString(R.string.navigation_logout), null));

        UserDataItem userdata = new UserDataItem(res.getDrawable(R.drawable.ic_user),
                MainActivity.account.Name,
                MainActivity.account.Type == 1 ? getString(R.string.type_student) : MainActivity.account.Type == 2 ? getString(R.string.type_professor) : getString(R.string.type_staff),
                MainActivity.account.Department);

        lvNavigation.setAdapter(new NavigationListAdapter(this, items).setUserDataItem(userdata));
        lvNavigation.setOnItemClickListener(new DrawerItemClickListener());

        createToggle();

        getFragmentManager().beginTransaction().add(R.id.container, new ApplicationListFragment()).commit();
    }

    private void createToggle()
    {
        toolbar = (Toolbar)findViewById(R.id.toolbar);
        dlDrawer = (DrawerLayout)findViewById(R.id.drawer_layout);

        setSupportActionBar(toolbar);
        dtToggle = new ActionBarDrawerToggle(this, dlDrawer, R.string.app_name, R.string.app_name);

        dlDrawer.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        dlDrawer.setDrawerListener(dtToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        // Sync the toggle state after onRestoreInstanceState has occurred.
        dtToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        dtToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (dtToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent e) {
        switch(keyCode) {
            case KeyEvent.KEYCODE_BACK:
                if(getFragmentManager().getBackStackEntryCount() == 0) {
                    long now = System.currentTimeMillis();
                    if(now - mBackkeyPushTime < 2500) {
                        finish();
                    }
                    else {
                        Toast.makeText(this, getString(R.string.toast_more_push_exit), Toast.LENGTH_SHORT).show();
                        mBackkeyPushTime = now;
                    }
                    return false;
                }
                else {
                    getFragmentManager().popBackStack();
                }
                return false;
        }

        return super.onKeyDown(keyCode, e);
    }
}
