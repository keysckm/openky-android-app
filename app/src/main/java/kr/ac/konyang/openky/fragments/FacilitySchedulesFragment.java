package kr.ac.konyang.openky.fragments;

import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Pair;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import kr.ac.konyang.openky.R;
import kr.ac.konyang.openky.model.ScheduleModel;
import kr.ac.konyang.openky.reflect.ModelGenerater;
import kr.ac.konyang.openky.requester.FacilityRequester;

/**
 * Created by KyeongMin on 2015-05-21.
 */
public class FacilitySchedulesFragment extends BaseFragment implements View.OnClickListener, Toolbar.OnMenuItemClickListener{
    @Override
    public boolean onMenuItemClick(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_request_application:
                if (period_list == null || period_list.isEmpty()) {
                    Toast.makeText(getActivity(), getString(R.string.error_not_select_period), Toast.LENGTH_SHORT).show();
                    break;
                }
                // application fragment mode setting
                bundle.putString("Mode", ApplicationFragment.Mode.Create.toString());

                bundle.putIntegerArrayList("Periods", period_list);
                bundle.putInt("Year", select_day.get(Calendar.YEAR));
                // JAVA Calendar 클래스는 Month의 Base가 0이다.
                // 즉 0 = 1월, 11 = 12월 이다.
                bundle.putInt("Month", select_day.get(Calendar.MONTH)+1);
                bundle.putInt("Day", select_day.get(Calendar.DATE));
                bundle.putString("Mode", ApplicationFragment.Mode.Create.toString());
                super.bundle = bundle;
                startFragment(getFragmentManager(), ApplicationFragment.class);
                break;

            case R.id.action_cancel_select: // 선택사항 초기화
                if (btn_list == null)
                    break;
                for (Button b : btn_list) {
                    Pair<Pair<Calendar, Integer>, Boolean> p = (Pair<Pair<Calendar, Integer>, Boolean>) b.getTag();
                    Calendar c = p.first.first;
                    int period = p.first.second;
                    boolean isSelected = p.second;
                    b.setTag(new Pair<Pair<Calendar, Integer>, Boolean>(new Pair<Calendar, Integer>(c, period), false));
                    b.setText("");
                }
                btn_list.clear();
                btn_list = null;
                period_list.clear();
                period_list = null;
                select_day = null;
                break;

            case R.id.action_refresh:
                row_list.clear();
                row_list = null;
                Refresh();
                break;
        }
        return false;
    }

    public class ScheduleGetTask extends FragmentAsyncTask<Integer, Void> {

        @Override
        protected Integer overrideDoInBackground(Integer... params) throws IOException, JSONException, NullPointerException {

            Calendar c = Calendar.getInstance();

            int status = 0;
            for (int i = 0; i < 7; i++) {
                Log.d("task", c.get(Calendar.YEAR) + "/" + (c.get(Calendar.MONTH) + 1) + "/" + c.get(Calendar.DATE) + "(" + c.get(Calendar.DAY_OF_MONTH) + ")");
                HttpResponse rp = FacilityRequester.RequestSchedule(params[0], c);
                status = rp.getStatusLine().getStatusCode();
                if (status == 200) {
                    String response_body = EntityUtils.toString(rp.getEntity(), HTTP.UTF_8);
                    Log.d("request", response_body + "code: " + rp.getStatusLine().getStatusCode());

                    JSONObject result = new JSONObject(response_body);
                    ScheduleModel s = (ScheduleModel) ModelGenerater.GenerateFromJson(ScheduleModel.class, result);

                    schedules.add(s);
                    c.add(Calendar.DATE, 1);
                }
            }
            return status;
        }

        @Override
        protected void overridePostExecute() {
            createViews();
        }
    }

    private ArrayList<ScheduleModel> schedules;
    private Calendar select_day;            // 선택 날짜
    private ArrayList<Integer> period_list; // 선택된 교시 정보
    private ArrayList<Button> btn_list;     // 선택된 버튼 배열
    private ArrayList<TableRow> row_list;   // 테이블 레이아웃의 열 데이터들(button이 붙어있음)
    private Bundle bundle;

    @Override
    public void onClick(View v) {
        Button btn = (Button)v;
        Pair<Pair<Calendar, Integer>, Boolean> p = (Pair<Pair<Calendar, Integer>, Boolean>)btn.getTag();
        Calendar c = p.first.first;
        int period = p.first.second;
        boolean isSelected = p.second;
        if(!isSelected) {
            if (select_day == null)
                select_day = c;
            else if(!(select_day.get(Calendar.YEAR) == c.get(Calendar.YEAR) &&
                    select_day.get(Calendar.MONTH) == c.get(Calendar.MONTH) &&
                    select_day.get(Calendar.DATE) == c.get(Calendar.DATE))) {
                Toast.makeText(getActivity(), getString(R.string.error_not_same_date), Toast.LENGTH_SHORT).show();
                return;
            }

            if (period_list == null)
                period_list = new ArrayList<Integer>(); // 첫 등록
            else {   // 기존에 데이터가 있음. 이 데이터는 오름차순 정렬 되어있음
                // 가장 빠른 시간과 가장 늦은 시간을 구한다.
                int period_min = period_list.get(0);
                int period_max = period_list.get(period_list.size() - 1);

                // 사용신청시 교시는 연속성이 있기 때문에 새로운 period는 period_min ~ period_max 범위 외의 값이다.
                if (period_min > period) {
                    if(period_min - period > 1) {
                        Toast.makeText(getActivity(), getString(R.string.error_not_continual_period), Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                else if(period_max < period) {
                    if(period - period_max > 1) {
                        Toast.makeText(getActivity(), getString(R.string.error_not_continual_period), Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
            }
            period_list.add(period);        // 교시 추가
            Collections.sort(period_list);  // 오름차순 정렬

            if(btn_list == null)
                btn_list = new ArrayList<Button>();
            btn_list.add(btn);

            btn.setTag(new Pair<Pair<Calendar, Integer>, Boolean>(new Pair<Calendar, Integer>(c, period), true));
            btn.setText(getString(R.string.btn_selected));
        }
        else {
            // 선택된 버튼을 다시 선택하면 색을 원래대로 돌려놓고 3번 tag값을 false로 한 후 period_list에서 값을 제거한다.
            // 이 떄, 만약 값이 period_list의 중간값이라면 해제할 수 없다.
            if(period != period_list.get(0) && period != period_list.get(period_list.size()-1)) // 첫번째 값도, 마지막 값도 아니니 중간값이다.
            {
                Toast.makeText(getActivity(), getString(R.string.error_not_cancel_middle), Toast.LENGTH_SHORT).show();
                return;
            }

            // 인덱스 찾는다.
            int index = period == period_list.get(0) ? 0 : period_list.size()-1;
            period_list.remove(index);  // 값 제거

            // btn 찾는다.
            for(int i = 0; i < btn_list.size(); i++)
            {
                if(btn_list.get(i) == btn) {
                    btn_list.remove(i);
                    break;
                }
            }

            if(period_list.isEmpty())   // 리스트가 비었다. -> 모든 버튼이 해제된 상태임
            {
                period_list = null;
                select_day = null;      // 모든 객체 해제
            }

            btn.setTag(new Pair<Pair<Calendar, Integer>, Boolean>(new Pair<Calendar, Integer>(c, period), false));
            btn.setText("");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_schedule_facility, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Toolbar t = (Toolbar)getActivity().findViewById(R.id.toolbar);
        if(t != null)
        {
            t.inflateMenu(R.menu.selectschedule);
            t.setOnMenuItemClickListener(this);
        }

        bundle = getArguments();
    }

    @Override
    public void onResume()
    {
        super.onResume();

        Refresh();
    }

    private void Refresh() {
        int facility = bundle.getInt("Facility");
        if(row_list == null) {
            schedules = new ArrayList<ScheduleModel>();
            new ScheduleGetTask().execute(facility);
        }
        else {
            createViews();
        }
    }

    @Override
    public void onDestroyView() {
        // back stack으로 돌아왔을 때, TableRow를 재활용하기 위해
        // TableLayout에 붙어있는 모든 TableRow를 제거한다.
        TableLayout layout = (TableLayout)getView().findViewById(R.id.schedule_table);
        layout.removeAllViews();

        super.onDestroyView();
    }

    private TextView createDp40TextView() {
        Resources r = getResources();
        int dp40 = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, r.getDisplayMetrics());

        TextView txtVoid = new TextView(getActivity());
        txtVoid.setWidth(dp40);
        txtVoid.setHeight(dp40);

        return txtVoid;
    }

    private Button createDp40Button() {
        Resources r = getResources();
        int dp40 = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 40, r.getDisplayMetrics());

        Button btnVoid = new Button(getActivity());
        btnVoid.setWidth(dp40);
        btnVoid.setHeight(dp40);

        return btnVoid;
    }

    private void createViews() {
        TableLayout layout = (TableLayout) getView().findViewById(R.id.schedule_table);
        layout.removeAllViews();

        TableRow row = new TableRow(getActivity());

        TextView txtVoid = createDp40TextView();
        row.addView(txtVoid);

        for (ScheduleModel s : schedules) {
            String date = s.Month + "/" + s.Day;
            TextView header = createDp40TextView();
            header.setText(date);
            header.setGravity(Gravity.CENTER_HORIZONTAL);
            row.addView(header);
        }
        layout.addView(row);

        if(row_list == null) {
            row_list = new ArrayList<TableRow>();
            for (int i = 1; i <= 15; i++) {
                TableRow btnRow = new TableRow(getActivity());
                TextView txtRow = createDp40TextView();
                txtRow.setText(String.valueOf(i));
                btnRow.addView(txtRow);
                for (ScheduleModel s : schedules) {
                    Button btn = createDp40Button();
                    btn.setEnabled(true);
                    btn.setOnClickListener(this);
                    Calendar c = Calendar.getInstance();
                    // Java의 Calendar의 Month는 0이 Base이다.
                    // Calendar값으로 Month를 넣을 때 1 빼주지 않으면
                    // 실제로 입력한 값 +1을 달로 인식한다.
                    // 31일 입력할 떄 문제가 발생할 수 있음.
                    c.set(s.Year, s.Month-1, s.Day);
                    // 튜플<튜플<날짜, 교시>, 선택됨> 형태
                    btn.setTag(new Pair<Pair<Calendar, Integer>, Boolean>(new Pair<Calendar, Integer>(c, i), false));
                    for (int period : s.Period) {
                        if (period == i)
                            btn.setEnabled(false);
                    }
                    btnRow.addView(btn);
                }
                layout.addView(btnRow);
                row_list.add(btnRow);
            }
        }
        else
        {
            for(TableRow r : row_list) {
                layout.addView(r);
            }
        }
    }
}