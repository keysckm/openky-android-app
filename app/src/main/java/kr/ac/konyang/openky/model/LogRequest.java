package kr.ac.konyang.openky.model;

/**
 * Created by KyeongMin on 2015-06-13.
 */
public class LogRequest extends AuthModel {
    public int Facility;
    public int Year;
    public int Month;
    public int Day;
}
