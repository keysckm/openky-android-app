package kr.ac.konyang.openky;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;

import kr.ac.konyang.openky.model.ApplicationDetailModel;

/**
 * Created by KyeongMin on 2015-05-23.
 */
public class MyApplicationItemView extends LinearLayout {

    TextView txtFacility;
    TextView txtPeople;
    TextView txtUsePeriod;
    TextView txtUseDate;

    ImageView ivApproval;

    TextView txtApplicationDate;

    public MyApplicationItemView(Context context) {
        super(context);

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.listitem_applicationlist, this, true);

        txtFacility = (TextView)findViewById(R.id.txtUseFacilityName);
        txtPeople = (TextView)findViewById(R.id.txtUsePeople);
        txtUsePeriod = (TextView)findViewById(R.id.txtUsePeriod);
        txtUseDate = (TextView)findViewById(R.id.txtUseDate);

        txtApplicationDate = (TextView)findViewById(R.id.txtApplicationDate);

        ivApproval = (ImageView)findViewById(R.id.ivApproval);
    }

    public void setContent(ApplicationDetailModel item)
    {
        txtFacility.setText(item.Facility);
        txtPeople.setText(String.valueOf(item.Peoples));
        txtUsePeriod.setText(String.valueOf(item.Period));
        txtUseDate.setText(item.Year + "-" + item.Month + "-" + item.Day);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        txtApplicationDate.setText(sdf.format(item.ApplicationDate.getTime()));

        ivApproval.setImageResource(item.Approval == 0 ? R.drawable.ic_question : item.Approval == 1 ? R.drawable.ic_ok : R.drawable.ic_reject);
    }
}
