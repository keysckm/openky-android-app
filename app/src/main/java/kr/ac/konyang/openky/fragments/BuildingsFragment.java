package kr.ac.konyang.openky.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;

import org.apache.http.HttpResponse;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import kr.ac.konyang.openky.R;
import kr.ac.konyang.openky.SimpleTextItemView;
import kr.ac.konyang.openky.model.BuildingModel;
import kr.ac.konyang.openky.reflect.ModelGenerater;
import kr.ac.konyang.openky.requester.BuildingRequester;

/**
 * Created by KyeongMin on 2015-05-20.
 */
public class BuildingsFragment extends BaseFragment implements AdapterView.OnItemClickListener{

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        bundle = new Bundle();
        bundle.putInt("Building", (int)id);
        bundle.putString("BuildingName", lstItems.get(position).Name);
        startFragment(getFragmentManager(), FacilitiesFragment.class);
    }

    // Adapters
    public class BuildingListAdapter extends BaseAdapter
    {
        private Context context;
        private List<BuildingModel> items;

        public BuildingListAdapter(Context context, List<BuildingModel> items)
        {
            this.context = context;
            this.items = items;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return items.get(position).Index;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            SimpleTextItemView v;
            if(convertView == null)
                v = new SimpleTextItemView(context);
            else
                v = (SimpleTextItemView)convertView;

            v.setName(items.get(position).Name);

            return v;
        }
    }

    private class BuildingTask extends FragmentAsyncTask<Void, Void>
    {
        @Override
        protected Integer overrideDoInBackground(Void... params) throws IOException, JSONException, NullPointerException {

            int status = 0;
            HttpResponse rp = BuildingRequester.RequestBuildings();
            ArrayList<BuildingModel> lst = new ArrayList<BuildingModel>();
            status = rp.getStatusLine().getStatusCode();
            if(status == 200)
            {
                String response_body = EntityUtils.toString(rp.getEntity(), HTTP.UTF_8);
                Log.d("request", response_body + "code: " + rp.getStatusLine().getStatusCode());

                JSONArray arry = new JSONArray(response_body);
                for(int i = 0; i<arry.length(); i++)
                {
                    JSONObject json = arry.getJSONObject(i);

                    BuildingModel b = (BuildingModel)ModelGenerater.GenerateFromJson(BuildingModel.class, json);
                    lst.add(b);
                }
            }
            lstItems = lst;
            return status;
        }

        @Override
        protected void overridePostExecute() {
            lvSimpleStringView.setAdapter(new BuildingListAdapter(getView().getContext(), lstItems));
        }
    }

    private ListView lvSimpleStringView;
    private ArrayList<BuildingModel> lstItems;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // inflater를 이용해서 프레그먼트 레이아웃을 메모리에 올리고 리턴함.
        return inflater.inflate(R.layout.fragment_list_buildings, null);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        lvSimpleStringView = (ListView)getView().findViewById(R.id.lvBuildings);
        lvSimpleStringView.setEmptyView(getView().findViewById(R.id.emptyContent));
        lvSimpleStringView.setOnItemClickListener(this);
        new BuildingTask().execute();
    }
}
