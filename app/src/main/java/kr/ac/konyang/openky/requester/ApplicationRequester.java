package kr.ac.konyang.openky.requester;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import kr.ac.konyang.openky.MainActivity;
import kr.ac.konyang.openky.model.AuthModel;
import kr.ac.konyang.openky.model.DecideApplicationModel;
import kr.ac.konyang.openky.model.RequestApplication;
import kr.ac.konyang.openky.reflect.NvprGenerater;

/**
 * Created by KyeongMin on 2015-05-22.
 */
public class ApplicationRequester extends BaseRequester {

    public static final String urlString = "api/Application";

    /***
     * 사용자의 시설물 사용신청서를 제출
     * @param model 사용신청서 데이터
     * @return Httpresponse
     */
    public static HttpResponse RequestSubmit(RequestApplication model) {
        HttpClient httpClient = new DefaultHttpClient();
        try{
            URI url = new URI(MainActivity.baseUrl + urlString);
            HttpPost post = new HttpPost();
            post.setURI(url);
            List<NameValuePair> nameValuePairs = NvprGenerater.generate(model);
            post.setEntity(new UrlEncodedFormEntity(nameValuePairs, "utf-8"));  // URL Encoding -> utf-8, 한글깨짐방지

            return httpClient.execute(post);

        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /***
     * 서버로부터 사용자의 신청서 데이터를 받아옴
     * @param model 사용자의 인증 정보
     * @return 결과 HttpResponse
     */
    public static HttpResponse RequestApplications(AuthModel model) {
        HttpClient httpClient = new DefaultHttpClient();
        try {
            URI url = new URI(MainActivity.baseUrl + urlString);
            HttpCustomPost get = new HttpCustomPost();
            get.setMethod("GET");
            get.setURI(url);
            List<NameValuePair> nvpr = NvprGenerater.generate(model);
            get.setEntity(new UrlEncodedFormEntity(nvpr, "utf-8"));

            return httpClient.execute(get);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static HttpResponse RequestAccept(DecideApplicationModel model) {
        HttpClient httpClient = new DefaultHttpClient();
        try {
            URI url = new URI(MainActivity.baseUrl + urlString);
            HttpCustomPost method = new HttpCustomPost();
            method.setURI(url);
            method.setMethod("ACCEPT");

            List<NameValuePair> nvpr = NvprGenerater.generate(model);
            method.setEntity(new UrlEncodedFormEntity(nvpr, "utf-8"));

            return httpClient.execute(method);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static HttpResponse RequestReject(DecideApplicationModel model) {
        HttpClient httpClient = new DefaultHttpClient();
        try {
            URI url = new URI(MainActivity.baseUrl + urlString);
            HttpCustomPost method = new HttpCustomPost();
            method.setURI(url);
            method.setMethod("REJECT");

            List<NameValuePair> nvpr = NvprGenerater.generate(model);
            method.setEntity(new UrlEncodedFormEntity(nvpr, "utf-8"));

            return httpClient.execute(method);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static HttpResponse RequestCancel(DecideApplicationModel model) {
        HttpClient httpClient = new DefaultHttpClient();
        try {
            URI url = new URI(MainActivity.baseUrl + urlString);
            HttpCustomPost method = new HttpCustomPost();
            method.setURI(url);
            method.setMethod("DELETE");

            List<NameValuePair> nvpr = NvprGenerater.generate(model);
            method.setEntity(new UrlEncodedFormEntity(nvpr, "utf-8"));

            return httpClient.execute(method);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
