package kr.ac.konyang.openky.requester;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.List;

import kr.ac.konyang.openky.MainActivity;
import kr.ac.konyang.openky.model.AuthModel;
import kr.ac.konyang.openky.model.LogRequest;
import kr.ac.konyang.openky.reflect.NvprGenerater;

/**
 * Created by KyeongMin on 2015-05-20.
 */
public class FacilityRequester extends BaseRequester {

    public static final String urlString = "api/Facility";

    public static HttpResponse RequestFacility(int building_idx)
    {
        HttpClient httpClient = new DefaultHttpClient();
        try {
            URI url = new URI(MainActivity.baseUrl + urlString + "/" + building_idx);
            HttpGet get = new HttpGet();
            get.setURI(url);

            return httpClient.execute(get);

        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static HttpResponse RequestSchedule(int facility_idx, Calendar cal)
    {
        HttpClient httpClient = new DefaultHttpClient();
        try {
            URI url = new URI(
                    MainActivity.baseUrl + urlString + "?facility=" +
                            facility_idx + "&year=" + cal.get(Calendar.YEAR) +
                            "&month=" + (cal.get(Calendar.MONTH)+1) +
                            "&day=" + cal.get(Calendar.DATE));

            HttpGet get = new HttpGet();
            get.setURI(url);

            return httpClient.execute(get);

        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public static HttpResponse RequestState(AuthModel m)
    {
        HttpClient httpClient = new DefaultHttpClient();
        try {
            URI url = new URI(MainActivity.baseUrl + urlString);
            HttpCustomPost method = new HttpCustomPost();
            method.setMethod("STATE");
            List<NameValuePair> nvpr = NvprGenerater.generate(m);
            method.setURI(url);
            method.setEntity(new UrlEncodedFormEntity(nvpr, "utf-8"));

            return httpClient.execute(method);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }
}
