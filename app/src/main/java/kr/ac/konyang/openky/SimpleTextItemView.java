package kr.ac.konyang.openky;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by KyeongMin on 2015-05-20.
 */
public class SimpleTextItemView extends LinearLayout {

    private TextView txtName;

    public SimpleTextItemView(Context context) {
        super(context);

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.listitem_buildinglist, this, true);

        txtName = (TextView)findViewById(R.id.txtBuilding);
    }

    public void setName(String name)
    {
        txtName.setText(name);
    }
}
