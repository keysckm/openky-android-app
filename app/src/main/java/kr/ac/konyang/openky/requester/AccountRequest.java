package kr.ac.konyang.openky.requester;

import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import kr.ac.konyang.openky.MainActivity;
import kr.ac.konyang.openky.model.LoginModelModel;
import kr.ac.konyang.openky.reflect.NvprGenerater;

/**
 * Created by KyeongMin on 2015-05-19.
 */
public class AccountRequest extends BaseRequester {

    public static final String urlString = "api/Account";

    public static HttpResponse RequestLogout(String token)
    {
        HttpClient httpClient = new DefaultHttpClient();
        try{
            URI url = new URI(MainActivity.baseUrl + urlString);
            HttpCustomPost logout = new HttpCustomPost();
            logout.setMethod("LOGOUT");
            logout.setURI(url);

            List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("Token", token));
            logout.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            return httpClient.execute(logout);
        }
        catch(URISyntaxException e) {
            Log.e("login", e.getLocalizedMessage());
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            Log.e("login", e.getLocalizedMessage());
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            Log.e("login", e.getLocalizedMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Log.e("login", e.getLocalizedMessage());
            e.printStackTrace();
        }

        return null;
    }

    public static HttpResponse RequestTokenLogin(String token)
    {
        HttpClient httpClient = new DefaultHttpClient();
        try{
            URI url = new URI(MainActivity.baseUrl + urlString);
            HttpCustomPost login = new HttpCustomPost();
            login.setMethod("TOKEN");
            login.setURI(url);

            List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("Token", token));
            login.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            return httpClient.execute(login);
        }
        catch(URISyntaxException e) {
            Log.e("login", e.getLocalizedMessage());
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            Log.e("login", e.getLocalizedMessage());
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            Log.e("login", e.getLocalizedMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Log.e("login", e.getLocalizedMessage());
            e.printStackTrace();
        }

        return null;
    }

    public static HttpResponse RequestLogin(LoginModelModel model)
    {
        HttpClient httpClient = new DefaultHttpClient();
        try{
            URI url = new URI(MainActivity.baseUrl + urlString);
            HttpCustomPost login = new HttpCustomPost();
            login.setMethod("LOGIN");
            login.setURI(url);

            List<NameValuePair> nameValuePairs = NvprGenerater.generate(model);
            login.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            return httpClient.execute(login);
        }
        catch(URISyntaxException e) {
            Log.e("login", e.getLocalizedMessage());
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            Log.e("login", e.getLocalizedMessage());
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            Log.e("login", e.getLocalizedMessage());
            e.printStackTrace();
        } catch (IOException e) {
            Log.e("login", e.getLocalizedMessage());
            e.printStackTrace();
        }

        return null;
    }
}