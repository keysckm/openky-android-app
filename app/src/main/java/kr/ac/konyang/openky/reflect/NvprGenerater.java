package kr.ac.konyang.openky.reflect;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by KyeongMin on 2015-05-27.
 * 리플렉션을 이용해 model로부터 NVPR 객체를 자동으로 생성해주는 제네레이터입니다.
 */
public class NvprGenerater {
    public static List<NameValuePair> generate(Object model) {
        Class<?> cls = model.getClass();
        Field[] fields = cls.getFields();
        List<NameValuePair> nvprs = new ArrayList<NameValuePair>();
        for(Field f : fields) {
            try {
                Class<?> type = f.getType();
                String key = f.getName();

                String data = String.valueOf(f.get(model));
                NameValuePair nvpr = new BasicNameValuePair(key, data);
                nvprs.add(nvpr);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        return nvprs;
    }
}
