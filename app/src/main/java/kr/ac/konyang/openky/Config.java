package kr.ac.konyang.openky;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by KyeongMin on 2015-05-27.
 */
public class Config {

    private final static String path = "config.json";

    private Context context = null;
    private JSONObject json = null;

    private boolean loadJson() {
        try {
            File file = new File(context.getFilesDir(), path);
            if (!file.exists()) {
                json = new JSONObject();
                return false;
            }

            FileInputStream fin = new FileInputStream(file);
            String str = streamToString(fin);
            fin.close();

            json = new JSONObject(str);
            return true;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return false;
    }

    private String streamToString(InputStream is) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        while((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    public Config(Context context) {
        this.context = context;
        loadJson();
    }

    public synchronized String readConfig(String key) {
        try {
            if(json == null)
                return null;

            return json.getString(key);
        } catch (JSONException e) {
            return null;
        }
    }

    public synchronized boolean writeConfig(String key, String data) {
        try {
            json.put(key, data);

            File fd = new File(context.getFilesDir(), path);
            FileOutputStream out = new FileOutputStream(fd);
            out.write(json.toString().getBytes());
            out.close();
            return true;

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
}
