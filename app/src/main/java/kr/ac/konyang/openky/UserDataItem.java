package kr.ac.konyang.openky;

import android.graphics.drawable.Drawable;

/**
 * Created by KyeongMin on 2015-05-19.
 */
public class UserDataItem {
    public Drawable icon;
    public String name;
    public String type;
    public String department;

    public UserDataItem(Drawable icon, String name, String type, String department)
    {
        this.icon = icon;
        this.name = name;
        this.type = type;
        this.department = department;
    }
}
